function pressed(buttonPressed) {
    var button;
    var xhttp = new XMLHttpRequest();
    var formData = new FormData();

    // disable buttons once one is pressed
    for (button = 1; button < 5; button++) {
        document.getElementById("button" + button).disabled = true;
        console.log('disabled button ' + button );
    }

    // Send request
    formData.append("option", buttonPressed);
    xhttp.open('post', '/insert');
    xhttp.send(formData)


    // add cookie so they cant reload browser and click again
    document.cookie = "pressed=True;max-age=60*60*24";

    console.log(buttonPressed + " pressed")
}

// check if button has been pressed in last 24 hours
window.onload = function checkCookies() {
    if (getCookie('pressed') === 'True') {
        console.log("Its time for you to leave");
        var button;
        for (button = 1; button < 5; button++) {
            document.getElementById("button" + button).disabled = true;
        }
    }
    else {
        console.log("No cookies detected sir");
    }
};


// thanks Paul from stackoverflow for this snippet
function getCookie(c_name)
{
   var i,x,y,ARRcookies=document.cookie.split(";");
   for (i=0; i<ARRcookies.length; i++)
   {
      x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
      y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
      x=x.replace(/^\s+|\s+$/g,"");
      if (x==c_name)
      {
        return unescape(y);
      }
   }
}