This is a small project to learn about using and managing docker 
containers and setting up a database. 

To log on to the admin page just copy the key that is generated at 
runtime and go to http://<ip or url>/admin/<key>.
