FROM alpine
COPY requirements.txt requirements.txt
RUN apk --update --no-cache add python3 py3-pip freetype-dev libpng-dev gcc python3-dev gfortran  build-base openblas-dev git
RUN git pull https://gitlab.com/ire4ever1190/kanothoot
WORKDIR kanothoot
RUN pip3 install --no-cache-dir -r requirements.txt
# will be 80 once I'm able to test it
RUN gunicorn -b 0.0.0.0:9090 app:app

