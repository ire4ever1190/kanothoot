import os
import io
from binascii import hexlify
import matplotlib.ticker
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.rcdefaults()
import numpy as np
from flask import Flask, render_template, make_response, request, send_file
from tinydb import TinyDB, Query, where

db = TinyDB('db.json')
app = Flask(__name__)
# Generates a 24 character long key if one isn't provided has a environment variable
try:
	adminKey = os.environ["key"]
except KeyError:
	adminKey = hexlify(os.urandom(24)).decode()

print(adminKey)


@app.route('/admin/<key>')
def admin(key):
	if key == adminKey:
		response = make_response(render_template('admin.html'))
		response.set_cookie("Key", adminKey)
		return response
	else:
		return 'Wrong Key'


@app.route('/')
def home():
	q = Query()
	number = len(db.all())
	jsonStr = db.get(q.number == number)
	option1 = jsonStr['option1']['option']
	option2 = jsonStr['option2']['option']
	option3 = jsonStr['option3']['option']
	option4 = jsonStr['option4']['option']
	title = jsonStr['name']
	return render_template('user.html',
			       title=title,
			       option1=option1,
			       option2=option2,
			       option3=option3,
			       option4=option4)


@app.route('/plots')
def plots():
	return render_template('data.html')


@app.route('/plot')
def plot():
	q = Query()
	plt.figure(figsize=(8, 20))
	for i in range(len(db.all())):
		jsonStr = db.get(q.number == i + 1)
		objects = (jsonStr['option1']['option'],
			   jsonStr['option2']['option'],
			   jsonStr['option3']['option'],
			   jsonStr['option4']['option'])
		yPos = np.arange(len(objects))
		counts = [jsonStr['option1']['count'],
			  jsonStr['option2']['count'],
			  jsonStr['option3']['count'],
			  jsonStr['option4']['count']
			  ]
		locator = matplotlib.ticker.MultipleLocator(1)
		plt.subplot(len(db.all()), 1, i + 1)
		plt.bar(yPos, counts, align='center', alpha=0.5)
		plt.gca().yaxis.set_major_locator(locator)
		plt.xticks(yPos, objects)
		plt.ylabel('Votes')
		plt.title(jsonStr['name'])

	plt.tight_layout()
	img = io.BytesIO()
	plt.savefig(img)
	img.seek(0)
	return send_file(img, mimetype='image/png')


@app.route('/admin/<key>/insert', methods=["POST"])
@app.route('/insert', methods=["POST"])
def insert(key=None):
	if request.headers.get('admin') == "True" and key == adminKey:
		option1 = request.form['option1']
		option2 = request.form['option2']
		option3 = request.form['option3']
		option4 = request.form['option4']
		name = request.form['name']
		number = len(db.all())
		number += 1
		db.insert({'name': name, 'option1': {
			'option': option1,
			'count': 0
		}, 'option2': {
			'option': option2,
			'count': 0
		}, 'option3': {
			'option': option3,
			'count': 0
		}, 'option4': {
			'option': option4,
			'count': 0
		}, 'number': number})
		return "Added options {}, num {}, {}, {} ,{} ,{}".format(name, number,
									 option1,
									 option2,
									 option3,
									 option4)
	q = Query()
	number = len(db.all())
	option = request.form['option']
	option = "option" + str(option)

	jsonStr = db.get(q.number == number)
	parsed = jsonStr[option]['count']
	orgOption = jsonStr[option]['option']
	db.update({option: {'option': orgOption, 'count': parsed + 1}}, (where('number') == number))
	jsonStr = db.get(q.number == number)
	parsed = jsonStr[option]
	return str(parsed)


if __name__ == '__main__':
	app.run()
